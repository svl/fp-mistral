..
 This work is licensed under a Creative Commons Attribution 3.0 Unported
 License.

 http://creativecommons.org/licenses/by/3.0/legalcode

==============================================================
Fuel plugin for OpenStack Mistral deployment
==============================================================

[Mistral](https://wiki.openstack.org/wiki/Mistral) is a workflow service. Most
business processes consist of multiple distinct interconnected steps that need
to be executed in a particular order in a distributed environment. One can
describe such process as a set of tasks and task relations and upload such
description to Mistral so that it executes them.

Problem description
===================

Fuel 6.1 distribution can not deploy OpenStack Mistral out of the box.

Proposed change
===============

Implement a Fuel plugin that will install and configure OpenStack Mistral
service on all the OpenStack controller nodes.

Alternatives
------------

None

Data model impact
-----------------

None

REST API impact
---------------

None

Upgrade impact
--------------

None

Security impact
---------------

None

Notifications impact
--------------------

None

Other end user impact
---------------------

OpenStack Mistral has [dashboard for
Horizon](https://github.com/openstack/mistral-dashboard) webinterface.
At the time of writing it is on early development stage and it is not supported
by the plugin.
The only user interface for Mistral provided by the plugin is
python-mistralclient (mistral CLI client).

Performance Impact
------------------

Since all Mistral service runs as a daemon on all the controller nodes, they will consume
resources from the nodes.

Other deployer impact
---------------------

At the time of writing the only Mistral release available is
[stable/kilo](https://github.com/openstack/mistral/tree/stable/kilo). It has
some dependencies which are not available in MOS 6.1 release. To workaround this
Mistral was packaged [1](https://review.fuel-infra.org/#/c/9139/)
[2](https://review.fuel-infra.org/#/c/9216/ ) along with virtualenv containing
all required dependencies and python-mistralclient.

python-mistralclient can not use Mistral service endpoints from Keystone
catalog. Additional /root/openrc.mistral file is created on controller nodes
during deployment and should be sourced before mistral CLI client is used.

Developer impact
----------------

None

Implementation
==============

Assignee(s)
-----------

Primary assignee:
  Serhii Lystopad <slystopad@mirantis.com> (deployment engineer)

Work Items
----------

* Implement the Fuel plugin.

* Implement the Puppet manifests.

* Testing.

* Write the documentation.

Dependencies
============

* Fuel 6.1 and higher.

Testing
=======

* Prepare a test plan.

* Test the plugin by deploying environments with all Fuel deployment modes.

Documentation Impact
====================

* Deployment Guide (how to prepare an
  environment for installation, how to install the plugin, how to deploy an
  OpenStack environment with the plugin).

* User Guide (which features the plugin provides, how to use them in the
  deployed OpenStack environment).

* Test Plan.

* Test Report.

References
==========

.. [#] https://github.com/openstack/mistral

.. [#] https://github.com/openstack/python-mistralclient

.. [#] https://github.com/openstack/mistral-dashboard

.. [#] https://blueprints.launchpad.net/fuel/+spec/role-as-a-plugin
