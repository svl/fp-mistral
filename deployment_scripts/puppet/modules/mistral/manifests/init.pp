#
# Copyright (C) 2013 eNovance SAS <licensing@enovance.com>
#
# Author: Emilien Macchi <emilien.macchi@enovance.com>
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# == Class: mistral
#
# Setups Mistral API/engine/executor.
#
# === Parameters
#
# [*package_ensure*]
#   (optional) Ensure state for package. Defaults to 'present'.
#
# [*enabled*]
#   (optional) Enable state for service. Defaults to 'true'.
#
# [*manage_service*]
#   (optional) Whether to start/stop the service
#   Defaults to true
#
# [api_bind_address] Address that mistral API binds to. Optional. Defaults to  '0.0.0.0'
# [public_address] Public address for Mistral endpoints. Required.
# [internal_address] Internal address for Mistral endpoints. Required.
# [auth_host] Address where keystone can be accessed by Mistral. Required.
# [sql_connection] Connection URI for DB. Required.

# EXAMPLE


class mistral (
  $package_ensure              = present,
  $enabled                     = true,
  $manage_service              = true,
  $admin_password              = undef,
  $admin_tenant_name           = 'services',
  $admin_user                  = 'mistral',
  $auth_host                   = '127.0.0.1',
  $auth_port                   = 35357,
  $auth_protocol               = 'http',
  $auth_uri                    = false,
  $identity_uri                = false,
  $amqp_host                   = '127.0.0.1',
  $amqp_port                   = '5673',
  $amqp_user                   = 'nova',
  $amqp_password               = 'nova',
  $amqp_engine_topic           = 'mistralengine',
  $amqp_executor_topic         = 'mistralexecutor',
  $api_bind_address            = '0.0.0.0',
  $api_port                    = '8989',
  $public_address              = undef,
  $internal_address             = false,
  $admin_address                = false,
  $mistral_public_address       = false,
  $mistral_internal_address     = false,
  $mistral_admin_address        = false,
  $use_syslog_rfc_format        = true,
  $use_syslog                   = true,
  $sql_connection               = false,
  $db_user                      = 'mistral',
  $db_password                  = 'ChangeMePlsease#123',
  $db_dbname                    = 'mistral',
  $db_allowed_hosts             = ['%']
) {

  include mistral::params

  Mistral_config<||> ~> Service['mistral-api-service']
  Mistral_config<||> ~> Service['mistral-engine-service']
  Mistral_config<||> ~> Service['mistral-executor-service']

  Class['mistral::keystone::auth'] -> Service['mistral-api-service']
  Class['mistral::keystone::auth'] -> Service['mistral-engine-service']
  Class['mistral::keystone::auth'] -> Service['mistral-executor-service']

  File<||> -> Class['mistral::db::mysql']

  Class['mistral::db::mysql'] -> Service['mistral-api-service']
  Class['mistral::db::mysql'] -> Service['mistral-engine-service']
  Class['mistral::db::mysql'] -> Service['mistral-executor-service']

  Package['mistral-kilo'] -> Exec<| title == 'mistral-db-manage upgrade head' |>

  group { 'mistral':
    ensure  => present,
    system  => true,
#    gid     => $mistral_group_id,
    before  => User['mistral'],
  }

  user { 'mistral':
    ensure     => present,
    system     => true,
    groups     => 'mistral',
    home       => '/var/lib/mistral',
    managehome => false,
    shell      => '/bin/false',
#    uid        => $mistral_user_id,
#    gid        => $mistral_group_id,
  }

  if $auth_uri {
    mistral_config { 'keystone_authtoken/auth_uri': value => $auth_uri; }
  } else {
    mistral_config { 'keystone_authtoken/auth_uri': value => "${auth_protocol}://${auth_host}:5000/v3"; }
  }

  if $identity_uri {
    mistral_config { 'keystone_authtoken/identity_uri': value => $identity_uri; }
  } else {
    mistral_config { 'keystone_authtoken/identity_uri': value => "${auth_protocol}://${auth_host}:${auth_port}/"; }
  }

  # I have to do all of this crazy munging b/c parameters are not
  # set procedurally in Pupet
  if($internal_address) {
    $internal_real = $internal_address
  } else {
    $internal_real = $public_address
  }
  if($admin_address) {
    $admin_real = $admin_address
  } else {
    $admin_real = $internal_real
  }
  if($mistral_public_address) {
    $mistral_public_real = $mistral_public_address
  } else {
    $mistral_public_real = $public_address
  }
  if($mistral_internal_address) {
    $mistral_internal_real = $mistral_internal_address
  } else {
    $mistral_internal_real = $internal_real
  }
  if($mistral_admin_address) {
    $mistral_admin_real = $mistral_admin_address
  } else {
    $mistral_admin_real = $admin_real
  }

  class { 'mistral::keystone::auth':
    auth_name        => $admin_user,
    password         => $admin_password,
    public_address   => $mistral_public_real,
    admin_address    => $mistral_admin_real,
    internal_address => $mistral_internal_real,
  }

  # Setup DB connection string
  if($sql_connection) {
    mistral_config {
      'database/connection': value => $sql_connection;
    }
  } else {
    mistral_config {
      'database/connection': ensure => absent;
    }
  }

  class { 'mistral::db::mysql':
    user          => $db_user,
    password      => $db_password,
    dbname        => $db_dbname,
    allowed_hosts => $db_allowed_hosts,
  }

  mistral_config {
    'api/host':                value => $api_bind_address;
    'api/port':                value => $api_port;
  }

  mistral_config {
    'engine/topic':       value => $amqp_engine_topic;
  }

  mistral_config {
    'executor/topic':       value => $amqp_executor_topic;
  }

  mistral_config {
    'oslo_messaging_rabbit/rabbit_host':      value => $amqp_host;
    'oslo_messaging_rabbit/rabbit_port':      value => $amqp_port;
    'oslo_messaging_rabbit/rabbit_userid':    value => $amqp_user;
    'oslo_messaging_rabbit/rabbit_password':  value => $amqp_password;
  }

  mistral_config {
    'keystone_authtoken/auth_protocol':       value => $auth_protocol;
    'keystone_authtoken/auth_version':        value => 'v3';
    'keystone_authtoken/admin_user':          value => $admin_user;
    'keystone_authtoken/admin_password':      value => $admin_password;
    'keystone_authtoken/admin_tenant_name':   value => $admin_tenant_name;
  }

  mistral_config {
    'DEFAULT/use_syslog_rfc_format':       value => $use_syslog_rfc_format;
    'DEFAULT/use_syslog':                  value => $use_syslog;
  }

  if $::mistral::params::server_package {
    Package['mistral'] -> Mistral_config<||>
    package { 'mistral':
      ensure  => $package_ensure,
      name    => $::mistral::params::server_package,
    }
  }

  ### Redundant because deb package is expected to create symlinks
  ## Create symlinks
  file { '/usr/local/bin/mistral':
    ensure => 'link',
    target => '/opt/mistral/bin/mistral',
  }
  file { '/usr/local/bin/mistral-db-manage':
    ensure => 'link',
    target => '/opt/mistral/bin/mistral-db-manage',
  }

  if $manage_service {
    if $enabled {
      $service_ensure = 'running'
    } else {
      $service_ensure = 'stopped'
    }
  }

  service { 'mistral-api-service':
    ensure  => $service_ensure,
    name    => $::mistral::params::api_service,
    enable  => $enabled,
  }

  service { 'mistral-engine-service':
    ensure  => $service_ensure,
    name    => $::mistral::params::engine_service,
    enable  => $enabled,
  }

  service { 'mistral-executor-service':
    ensure  => $service_ensure,
    name    => $::mistral::params::executor_service,
    enable  => $enabled,
  }

  class { 'mistral::firewall': api_port => $api_port, }
  class { 'mistral::ha::haproxy':
    controllers              => hiera('controllers'),
    public_virtual_ip        => hiera('public_vip'),
    internal_virtual_ip      => hiera('management_vip'),
  }
  class { 'mistral::logging': }

}
