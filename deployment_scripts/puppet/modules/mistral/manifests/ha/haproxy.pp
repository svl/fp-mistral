#
class mistral::ha::haproxy (
  $controllers         = undef,
  $public_virtual_ip   = undef,
  $internal_virtual_ip = undef,
) {

  Haproxy::Service        { use_include => true }
  Haproxy::Balancermember { use_include => true }

  Openstack::Ha::Haproxy_service {
    server_names        => filter_hash($controllers, 'name'),
    ipaddresses         => filter_hash($controllers, 'internal_address'),
    public_virtual_ip   => $public_virtual_ip,
    internal_virtual_ip => $internal_virtual_ip,
  }

  openstack::ha::haproxy_service { 'mistral-api':
    # after all
    order                  => '333',
    listen_port            => 8989,
    public                 => true,
    require_service        => 'mistral-api',
    haproxy_config_options => {
        'option'         => ['httplog','httpclose'],
        'timeout server' => '11m',
    },
    balancermember_options => 'check inter 10s fastinter 2s downinter 3s rise 3 fall 3',
  }
}
