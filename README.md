OpenStack Mistral Plugin for Fuel
============

Fule plugin for OpenStack Mistral service deployment

Overview
--------

The plugin deploys Mistral services on controller nodes in Fuel MOS
environments.

[Mistral](https://wiki.openstack.org/wiki/Mistral) is a workflow service. Most
business processes consist of multiple distinct interconnected steps that need
to be executed in a particular order in a distributed environment. One can
describe such process as a set of tasks and task relations and upload such
description to Mistral so that it takes care of state management, correct
execution order, parallelism, synchronization and high availability. Mistral
also provides flexible task scheduling so that we can run a process according
to a specified schedule (i.e. every Sunday at 4.00pm) instead of running it
immediately. We call such set of tasks and relations between them a workflow. 

Requirements
------------

| Requirement                    | Version/Comment                            |
| ------------------------------ | ------------------------------------------ |
| Mirantis OpenStack compatility | 6.1                                        |
| Controller role assigned       | Plugin requires at least 1 controller node exists in Fuel environment |

Limitations
-----------

OpenStack Mistral services require AMQP service and SQL database. Plugin does
not deploy RDBMS (MySQL/Galera/Postgres etc.) itself. It expects AQMP and RDBMS
are deployed on previous deployment steps and plugin will fail if for example
RabbitMQ or Galera is not up and running.

The plugin does not deploy mistral dashboard for Horizon.

Installation Guide
==================

**Fuel Mistral Plugin** installation
-------------------------------------

To install Mistral Plugin for Fuel, follow these steps:

1. Clone plugin code from https://gitlab.com/svl/fp-mistral.git and build plugin RPM package

    ```
    > git clone https://gitlab.com/svl/fp-mistral.git
    > cd fp-mistral
    > fpb --build .
    ```
Refer to [How to build Fuel plugin](https://wiki.openstack.org/wiki/Fuel/Plugins#How_To:_Build_and_install_a_plugin_from_source) section for additional info

2. Copy the plugin package to the Fuel Master node.

    ```
    scp fuel-plugin-mistral-2.0-2.0.1-1.noarch.rpm root@<Fuel Master node IP address>:
    ```

3. Install the plugin using the `fuel` client:

    ```
    fuel plugins --install fuel-plugin-mistral-2.0-2.0.1-1.noarch.rpm
    ```

4. Verify that the plugin is installed correctly:

    ```
    fuel plugins --list
    ```

Please refer to the [Fuel Plugins
wiki](https://wiki.openstack.org/wiki/Fuel/Plugins) if you want to build the
plugin by yourself, version 2.0.0 (or higher) of the Fuel Plugin Builder is
required.

User Guide
==========

**Fuel Mistral Plugin** configuration
--------------------------------------

1. Create a new environment with the Fuel UI wizard.
2. Add at least 1 controller node
3. Select Settings tab of the Fuel web UI.
4. Scroll down the page, select the Mistral plugin checkbox and
fill-in the required fields.

**Using Mistral**
--------------------------------------
1. Login to one of the controller nodes

    ```
    fuel# ssh controller1
    ```

2. Specify Mistral API URL to be used by python-mistralclient

    ```
    controller1# . /root/openrc.mistral
    ```

3. Play with mistral

    ```
    controller1# mistral workflow-list
    controller1# mistral action-list
    ```

Known issues
------------

* Deployment will fail if Keystone is configured with read-only backend
* python-mistralclient can not enpoints from Keystone service catalog, so
 OS_MISTRAL_URL environment variable should contain URL on which Mistral API is
 available 

Release Notes
-------------

**2.0.1**

* Initial release of the plugin.

Development
===========

The *OpenStack Development Mailing List* is the preferred way to communicate,
emails should be sent to `openstack-dev@lists.openstack.org` with the subject
prefixed by `[fuel][plugins]`.

Reporting Bugs
--------------

Bugs should be filled on the [Launchpad fuel-plugins project](
https://bugs.launchpad.net/fuel-plugins) (not GitHub) with the tag `mistral`.

Contributing
------------

If you would like to contribute to the development of this Fuel plugin you must
follow the [OpenStack development workflow](
http://docs.openstack.org/infra/manual/developers.html#development-workflow).

Contributors
------------

* Serhii Lystopad <slystopad@mirantis.com>

