# == Class: mistral::keystone::auth
#
# Creates mistral endpoints and service account in keystone
#
# === Parameters:
#
# [*password*]
#   Password to create for the service user
#
# [*auth_name*]
#   (optional) The name of the mistral service user
#   Defaults to 'nova'
#
# [*public_address*]
#   (optional) The public mistral-api endpoint
#   Defaults to '127.0.0.1'
#
# [*admin_address*]
#   (optional) The admin mistral-api endpoint
#   Defaults to '127.0.0.1'
#
# [*internal_address*]
#   (optional) The internal mistral-api endpoint
#   Defaults to '127.0.0.1'
#
# [*api_port*]
#   (optional) The port to use for the compute endpoint
#   Defaults to '8989'
#
# [*api_version*]
#   (optional) The version of the workflow api to put in the endpoint
#   Defaults to 'v2'
#
# [*region*]
#   (optional) The region in which to place the endpoints
#   Defaults to 'RegionOne'
#
# [*tenant*]
#   (optional) The tenant to use for the nova service user
#   Defaults to 'services'
#
# [*email*]
#   (optional) The email address for the nova service user
#   Defaults to 'nova@localhost'
#
# [*configure_endpoint*]
#   (optional) Whether to create the endpoint.
#   Defaults to true
#
# [*public_protocol*]
#   (optional) Protocol to use for the public endpoint. Can be http or https.
#   Defaults to 'http'
#
# [*admin_protocol*]
#   Protocol for admin endpoints. Defaults to 'http'.
#
# [*internal_protocol*]
#   Protocol for internal endpoints. Defaults to 'http'.
#
class mistral::keystone::auth(
  $password               = undef,
  $auth_name              = 'mistral',
  $public_address         = '127.0.0.1',
  $admin_address          = '127.0.0.1',
  $internal_address       = '127.0.0.1',
  $api_port               = '8989',
  $api_version            = 'v2',
  $region                 = 'RegionOne',
  $tenant                 = 'services',
  $email                  = 'mistral@localhost',
  $public_protocol        = 'http',
  $configure_endpoint     = true,
  $admin_protocol         = 'http',
  $internal_protocol      = 'http'
) {

  Keystone_endpoint["${region}/${auth_name}"] ~> Service <| name == 'mistral-api' |>

  keystone_user { $auth_name:
    ensure   => present,
    password => $password,
    email    => $email,
    tenant   => $tenant,
  }
  keystone_user_role { "${auth_name}@${tenant}":
    ensure  => present,
    roles   => 'admin',
  }
  keystone_service { $auth_name:
    ensure      => present,
    type        => 'workflowv2',
    description => 'Openstack Mistral Service',
  }

  if $configure_endpoint {
    keystone_endpoint { "${region}/${auth_name}":
      ensure       => present,
      public_url   => "${public_protocol}://${public_address}:${api_port}/${api_version}",
      admin_url    => "${admin_protocol}://${admin_address}:${api_port}/${api_version}",
      internal_url => "${internal_protocol}://${internal_address}:${api_port}/${api_version}",
    }
  }

}
