class mistral::logging (
  $service_ensure = running,
  $enabled        = true,
) {

  include ::rsyslog::params
  include ::mistral::params

  File["${rsyslog::params::rsyslog_d}89-mistral.conf"] ~> Service[$rsyslog::params::service_name]
  File[$mistral::params::logfile] ~> Service[$rsyslog::params::service_name]

  file { "${rsyslog::params::rsyslog_d}89-mistral.conf":
    content => template('mistral/89-mistral.conf.erb'),
  } ->
  file { $mistral::params::logfile:
    ensure => file,
    owner  => 'syslog',
    group  => 'syslog',
  }->
  service { 'syslog-service':
    ensure  => $service_ensure,
    name    => $::rsyslog::params::service_name,
    enable  => $enabled,
  }

}
