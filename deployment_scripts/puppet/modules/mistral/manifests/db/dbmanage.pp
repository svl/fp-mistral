#
# Class to execute "mistral-db-manage populate"
#
class mistral::db::dbmanage {
  exec { 'mistral-db-manage upgrade head':
      path           => '/usr/local/bin',
      user           => 'mistral',
      refreshonly    => true,
      subscribe      => [Package['mistral-kilo'], Mistral_config['database/connection']],
      require        => User['mistral'],
  } ->
  exec { 'mistral-db-manage populate':
      path           => '/usr/local/bin',
      user           => 'mistral',
      refreshonly    => true,
      subscribe      => [Package['mistral-kilo'], Mistral_config['database/connection']],
      require        => User['mistral'],
  }
}

