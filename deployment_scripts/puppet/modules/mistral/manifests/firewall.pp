class mistral::firewall (
  $api_port = undef,
) {
  firewall {'150 mistral':
    port      => $api_port,
    proto     => 'tcp',
    action    => 'accept',
  }
}
