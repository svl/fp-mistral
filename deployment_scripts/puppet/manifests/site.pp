$fuel_settings = parseyaml($astute_settings_yaml)

$mysql_hash           = $::fuel_settings['mysql']
$rabbit_hash          = $::fuel_settings['rabbit']
$mistral_hash         = $::fuel_settings['fuel-plugin-mistral']

class { '::mistral':
  admin_user       => $mistral_hash['mistral_keystone_user'],
  admin_password   => $mistral_hash['mistral_keystone_password'],
  auth_host        => $::fuel_settings['management_vip'],
#  amqp_hosts      => $amqp_hosts,
  amqp_user        => $rabbit_hash['user'],
  amqp_password    => $rabbit_hash['password'],
  api_bind_address => $::ipaddress_br_mgmt,
  public_address   => $::fuel_settings['public_vip'],
  internal_address => $::fuel_settings['management_vip'],
  sql_connection   => "mysql://${mistral_hash['mistral_db_user']}:${mistral_hash['mistral_db_password']}@${::fuel_settings['management_vip']}/mistral?charset=utf8&read_timeout=60",
  db_password      => $mistral_hash['mistral_db_password'],
  db_user          => $mistral_hash['mistral_db_user'],
}
