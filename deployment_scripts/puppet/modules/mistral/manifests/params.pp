#    Copyright 2015 Mirantis, Inc.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

#This class contains necessary parameters for all other manifests

class mistral::params {

  if($::osfamily == 'Debian') {

    $server_package       = 'mistral-kilo'
    $api_service          = 'mistral-api'
    $engine_service       = 'mistral-engine'
    $executor_service     = 'mistral-executor'
    $logfile              = '/var/log/mistral-all.log'

  } else {
    fail("Unsupported osfamily ${::osfamily}")
  }

}
